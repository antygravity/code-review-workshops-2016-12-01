package eu.espeo.workshops.cr.stats;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.espeo.workshops.cr.repository.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import java.util.UUID;

@Value
@Builder
@AllArgsConstructor
public class Stats implements Entity<UUID> {

    @Wither
    UUID reviewId;
    int loc;
    int comments;
    int resolved;

    @Override
    @JsonIgnore
    public UUID getId() {
        return reviewId;
    }
}
