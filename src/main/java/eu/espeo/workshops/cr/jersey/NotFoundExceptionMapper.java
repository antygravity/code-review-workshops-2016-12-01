package eu.espeo.workshops.cr.jersey;

import static eu.espeo.workshops.cr.jersey.Error.errorMessage;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import eu.espeo.workshops.cr.jersey.NotFoundExceptionMapper.NotFoundException;
import lombok.NoArgsConstructor;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException exception) {
        return Response.status(NOT_FOUND)
                .entity(errorMessage(NOT_FOUND.getReasonPhrase()))
                .type(APPLICATION_JSON_TYPE)
                .build();
    }

    @NoArgsConstructor
    public static class NotFoundException extends RuntimeException {
    }
}
