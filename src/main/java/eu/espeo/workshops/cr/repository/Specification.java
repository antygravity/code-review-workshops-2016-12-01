package eu.espeo.workshops.cr.repository;

@FunctionalInterface
public interface Specification<T extends Entity> {

    boolean matches(T entity);

    static <T extends Entity> Specification<T> all() {
        return e -> true;
    }
}
